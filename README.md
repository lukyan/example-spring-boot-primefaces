# spring-boot application with primefaces dataTable CRUD

Clone project:
```
git clone https://lukyan@bitbucket.org/lukyan/example-spring-boot-primefaces.git
```
Start mysql docker container:
```
cd src/main/docker
docker-compose up
```
Or install mysql server and change application properties
according to your database settings.
 
Start application in debug mode:
```
./mvnw clean spring-boot:run
```
Or build application and run unit tests:
```
./mvnw clean install
```
After build start application:
```
java -jar target/example-spring-boot-primefaces-0.1.0-SNAPSHOT.jar
```
Open url in your preferred browser:

[http://localhost:8080](http://localhost:8080)


