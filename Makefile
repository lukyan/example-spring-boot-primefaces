.DEFAULT_GOAL := debug

build-run: clean build run

build:
	echo "" > build.log
	./mvnw clean install | tee build.log

run:
	echo "" > run.log
	java -jar ./target/example-spring-boot-primefaces-0.1.0-SNAPSHOT.jar | tee run.log

debug:
	echo "" > debug.log
	./mvnw clean spring-boot:run -DskipTests | tee debug.log

compile:
	echo "" > compile.log
	./mvnw compile | tee compile.log

dep:
	echo "" > dependency.log
	./mvnw dependency:tree | tee dependency.log

