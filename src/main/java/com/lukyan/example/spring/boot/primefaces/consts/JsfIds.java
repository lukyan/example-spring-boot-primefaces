package com.lukyan.example.spring.boot.primefaces.consts;

public interface JsfIds {
    String messages = "messages";
    String contactsForm = "contactsForm";
    String contactList = ":contactsForm:contactList";
}
