package com.lukyan.example.spring.boot.primefaces.bean;

import com.lukyan.example.spring.boot.primefaces.consts.DialogParams;
import com.lukyan.example.spring.boot.primefaces.consts.JsfIds;
import com.lukyan.example.spring.boot.primefaces.consts.Views;
import com.lukyan.example.spring.boot.primefaces.converters.ContactToStringConverter;
import com.lukyan.example.spring.boot.primefaces.helpers.DlgHelper;
import com.lukyan.example.spring.boot.primefaces.jpa.entity.Contact;
import com.lukyan.example.spring.boot.primefaces.services.api.ContactService;
import lombok.NonNull;
import lombok.Setter;
import org.primefaces.PrimeFaces;
import org.primefaces.event.RowEditEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.time.Instant;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Named
@ViewScoped
public class ContactsBean implements Serializable {

    private static final long serialVersionUID = 1L;

    private static final Logger log = LoggerFactory.getLogger(ContactsBean.class);

    @Inject
    private ContactService contactService;

    @Inject
    private ContactToStringConverter contactToStringConverter;

    @Setter
    private List<Contact> contacts;

    @Setter
    private Contact contact = new Contact();

    @PostConstruct
    public void init() {
        contacts = contactService.findAll();
    }

    public List<Contact> getContacts() {
        contacts = contactService.findAll();
        return this.contacts;
    }

    public Contact getContact() {
        return this.contact;
    }

    public void delete(Contact contact) {
        contactService.delete(contact);
        contacts.remove(contact);
        FacesContext.getCurrentInstance().addMessage(JsfIds.messages,
                new FacesMessage(FacesMessage.SEVERITY_INFO, "Success:", "Delete successful"));
    }

    public void showAddContactDialog() {
        PrimeFaces.current().executeScript("PF('addContactDialogVar').show();");
    }

    public void showEditContactDialog(@NonNull Contact contactParam) {
        String contactAsParam = contactToStringConverter.convert(contactParam);
        Map<String, List<String>> params = new HashMap<>();
        params.put(DialogParams.contact, Arrays.asList(contactAsParam));
        PrimeFaces.current().dialog().openDynamic(Views.edit, DlgHelper.instance().options(), params);
    }

    public void updateContact() {
        edit();
        // TODO don't work - fix it
        FacesContext.getCurrentInstance().addMessage(JsfIds.messages,
                new FacesMessage(FacesMessage.SEVERITY_INFO, "Success:", "Update successful"));
        PrimeFaces.current().dialog().closeDynamic(Views.edit);
    }

    public void add() {
        addEdit(false);
    }

    public void edit() {
        addEdit(true);
    }

    private void addEdit(boolean isEdit) {
        try {
            Contact newContact = contact.clone();
            Instant now = Instant.now();
            if (!isEdit) {
                newContact.setId(null);
                newContact.setCreatedAt(now);
            }
            newContact.setUpdatedAt(now);
            contact = contactService.save(newContact);
            contacts = contactService.findAll();
            FacesContext.getCurrentInstance().addMessage(JsfIds.messages,
                    new FacesMessage(FacesMessage.SEVERITY_INFO, "Success:", "Save successful"));
        } catch (Throwable throwable) {
            log.error(throwable.getMessage(), throwable);
            FacesContext.getCurrentInstance().addMessage(JsfIds.messages, new FacesMessage(FacesMessage.SEVERITY_ERROR,
                    "Store to database error with message:", throwable.getMessage()));
        }
    }

    public void onRowEdit(RowEditEvent event) {
        try {
            Contact contact = (Contact) event.getObject();
            contactService.save(contact);
            log.info("The Contact {} Is Edited Successfully", this.contact);
        } catch (Throwable throwable) {
            log.error(throwable.getMessage(), throwable);
            FacesContext.getCurrentInstance().addMessage(JsfIds.messages, new FacesMessage(FacesMessage.SEVERITY_ERROR,
                    "Delete from database error with message:", throwable.getMessage()));
        }
    }
}