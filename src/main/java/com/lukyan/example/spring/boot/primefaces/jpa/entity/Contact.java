package com.lukyan.example.spring.boot.primefaces.jpa.entity;

import com.rits.cloning.Cloner;
import lombok.AccessLevel;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.FieldDefaults;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "contacts")
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class Contact extends AbstractEntity {

    private static final long serialVersionUID = 1L;

    @Getter
    @Setter
    @Column(name = "first_name", nullable = false)
    String firstName;

    @Getter
    @Setter
    @Column(name = "last_name", nullable = false)
    String lastName;

    @Getter
    @Setter
    @Column(name = "phone", nullable = false)
            String phone;
    public Contact(String firstName, String lastName, String phone) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.phone = phone;
    }

    @Override
    public Contact clone() {
        Cloner cloner = new Cloner();
        Contact cloned = cloner.deepClone(this);
        return cloned;
    }
}
