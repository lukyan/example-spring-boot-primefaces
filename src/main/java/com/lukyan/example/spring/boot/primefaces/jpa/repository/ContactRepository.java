package com.lukyan.example.spring.boot.primefaces.jpa.repository;

import com.lukyan.example.spring.boot.primefaces.jpa.entity.Contact;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ContactRepository extends JpaRepository<Contact, Long> {
}
