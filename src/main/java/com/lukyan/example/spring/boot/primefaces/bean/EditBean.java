package com.lukyan.example.spring.boot.primefaces.bean;

import com.lukyan.example.spring.boot.primefaces.consts.DialogParams;
import com.lukyan.example.spring.boot.primefaces.converters.ContactToStringConverter;
import com.lukyan.example.spring.boot.primefaces.converters.StringToContactConverter;
import com.lukyan.example.spring.boot.primefaces.jpa.entity.Contact;
import lombok.Setter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;

@Named
@RequestScoped
public class EditBean implements Serializable {

    private static final long serialVersionUID = 1L;

    private static final Logger log = LoggerFactory.getLogger(EditBean.class);

    @Setter
    private Contact contact = new Contact();

    @Inject
    private StringToContactConverter stringToContactConverter;

    @Inject
    private ContactToStringConverter contactToStringConverter;

    public Contact getContact() {
        String contactParam = FacesContext.getCurrentInstance()
                .getExternalContext()
                .getRequestParameterMap().get(DialogParams.contact);
        if (contactParam != null) {
            Contact contact = stringToContactConverter.convert(contactParam);
            this.contact = contact;
        }
        return contact;
    }
}
