package com.lukyan.example.spring.boot.primefaces.helpers;

import lombok.NonNull;

import java.time.Instant;

public class InstantHelper {

    private InstantHelper() {
    }

    private static class SingletonHolder {
        static final InstantHelper INSTANCE = new InstantHelper();
    }

    public static InstantHelper instance() {
        return SingletonHolder.INSTANCE;
    }

    public Instant convert(@NonNull String data) {
        return Instant.parse(data);
    }

    public String convert(@NonNull Instant instant) {
        return instant.toString();
    }
}
