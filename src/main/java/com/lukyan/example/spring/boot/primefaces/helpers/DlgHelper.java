package com.lukyan.example.spring.boot.primefaces.helpers;

import com.lukyan.example.spring.boot.primefaces.consts.DlgOption;

import java.util.HashMap;
import java.util.Map;

public class DlgHelper {

    private DlgHelper() {
    }

    private static class SingletonHolder {
        static final DlgHelper INSTANCE = new DlgHelper();
    }

    public static DlgHelper instance() {
        return SingletonHolder.INSTANCE;
    }

    public Map<String, Object> options() {
        Map<String, Object> options = new HashMap<>();
        options.put(DlgOption.resizable, false);
        options.put(DlgOption.draggable, true);
        options.put(DlgOption.modal, true);
/*
        options.put(DlgOption.height, 400);
        options.put(DlgOption.contentHeight, "100%");
*/
        return options;
    }
}
