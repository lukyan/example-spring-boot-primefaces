package com.lukyan.example.spring.boot.primefaces.services.impl;

import com.lukyan.example.spring.boot.primefaces.jpa.entity.Contact;
import com.lukyan.example.spring.boot.primefaces.jpa.repository.ContactRepository;
import com.lukyan.example.spring.boot.primefaces.jpa.repository.CustomContactRepository;
import com.lukyan.example.spring.boot.primefaces.services.api.ContactService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("contactService")
public class ContactServiceImpl implements ContactService {

    private final static Logger log = LoggerFactory.getLogger(ContactServiceImpl.class);

    @Autowired
    private ContactRepository contactRepository;

    @Autowired
    private CustomContactRepository customContactRepository;

    @Override
    public Contact getOne(Long id) {
        return contactRepository.getOne(id);
    }

    @Override
    public List<Contact> findAll(String sortBy, String sortOrder, Integer offset, Integer limit, String keyword) {
        return customContactRepository.findAll(sortBy, sortOrder, offset, limit, keyword);
    }

    @Override
    public List<Contact> findAll() {
        return contactRepository.findAll();
    }

    @Override
    public void delete(Long id) {
        contactRepository.deleteById(id);
    }

    @Override
    public void delete(Contact contact) {
        contactRepository.delete(contact);
    }

    @Override
    public Contact save(Contact contact) {
        return contactRepository.save(contact);
    }

    @Override
    public Long count() {
        return contactRepository.count();
    }
}
