package com.lukyan.example.spring.boot.primefaces.consts;

public interface ContactFields {
    String ID = "id";
    String NAME = "name";
    String LAST_NAME = "lastName";
    String PHONE = "phone";
    String CREATED_AT = "createdAt";
    String UPDATED_AT = "updatedAt";
}
