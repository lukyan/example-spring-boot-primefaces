package com.lukyan.example.spring.boot.primefaces.jpa.entity;

import com.lukyan.example.spring.boot.primefaces.jpa.entity.converter.JPAInstantConverter;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.FieldDefaults;

import javax.persistence.*;
import java.io.Serializable;
import java.time.Instant;

@NoArgsConstructor
@MappedSuperclass
@FieldDefaults(level = AccessLevel.PRIVATE)
public abstract class AbstractEntity implements Serializable {

    @Getter
    @Setter
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    @Getter
    @Setter
    @Column(name = "created_at", nullable = false, columnDefinition = "TIMESTAMP")
    @Convert(converter = JPAInstantConverter.class)
    Instant createdAt = Instant.now();

    @Getter
    @Setter
    @Column(name = "updated_at", nullable = false, columnDefinition = "TIMESTAMP")
    @Convert(converter = JPAInstantConverter.class)
    Instant updatedAt = Instant.now();
}
