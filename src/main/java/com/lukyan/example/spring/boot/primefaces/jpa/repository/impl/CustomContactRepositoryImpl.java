package com.lukyan.example.spring.boot.primefaces.jpa.repository.impl;

import com.lukyan.example.spring.boot.primefaces.consts.ContactFields;
import com.lukyan.example.spring.boot.primefaces.consts.SortOrder;
import com.lukyan.example.spring.boot.primefaces.jpa.entity.Contact;
import com.lukyan.example.spring.boot.primefaces.jpa.repository.CustomContactRepository;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.List;

@Repository
public class CustomContactRepositoryImpl implements CustomContactRepository {

    @PersistenceContext
    private EntityManager entityManager;

    private final static String DEFAULT_SORT_BY = ContactFields.CREATED_AT;

    @Value("${default.limit}")
    private Integer defaultLimit;

    @Override
    public List<Contact> findAll(String sortBy, String sortOrder, Integer offset, Integer limit, String keyword) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Contact> criteriaQuery = criteriaBuilder.createQuery(Contact.class);
        Root<Contact> from = criteriaQuery.from(Contact.class);
        keyword = keyword != null ? keyword.toLowerCase() : null;
        sortBy = sortBy != null ? sortBy : DEFAULT_SORT_BY;
        if (sortOrder == null) {
            switch (sortBy) {
                case ContactFields.NAME:
                    sortOrder = SortOrder.ASC;
                    break;
                case ContactFields.LAST_NAME:
                    sortOrder = SortOrder.ASC;
                    break;
                case ContactFields.CREATED_AT:
                    sortOrder = SortOrder.DESC;
                    break;
                case ContactFields.UPDATED_AT:
                    sortOrder = SortOrder.DESC;
                    break;
            }
        }
        if (SortOrder.ASC.equals(sortOrder.toLowerCase())) {
            criteriaQuery = criteriaQuery.orderBy(criteriaBuilder.asc(from.get(sortBy)));
        } else if (SortOrder.DESC.equals(sortOrder.toLowerCase())) {
            criteriaQuery = criteriaQuery.orderBy(criteriaBuilder.desc(from.get(sortBy)));
        }
        if (keyword != null) {
            Predicate namePredicate = criteriaBuilder.like(criteriaBuilder.lower(from.get(ContactFields.NAME)),
                    "%" + keyword.toLowerCase() + "%");
            Predicate lastNamePredicate = criteriaBuilder.like(criteriaBuilder.lower(from.get(ContactFields.LAST_NAME)),
                    "%" + keyword.toLowerCase() + "%");
            Predicate phonePredicate = criteriaBuilder.like(criteriaBuilder.lower(from.get(ContactFields.PHONE)),
                    "%" + keyword.toLowerCase() + "%");
            criteriaQuery = criteriaQuery.where(criteriaBuilder.or(namePredicate,
                    lastNamePredicate, phonePredicate));
        }
        offset = offset != null ? offset : 0;
        limit = limit != null ? limit : defaultLimit;
        TypedQuery<Contact> query = entityManager.createQuery(criteriaQuery).setFirstResult(offset).setMaxResults(limit);
        return query.getResultList();
    }
}