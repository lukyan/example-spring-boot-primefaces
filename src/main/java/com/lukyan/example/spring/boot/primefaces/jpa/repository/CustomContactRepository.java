package com.lukyan.example.spring.boot.primefaces.jpa.repository;

import com.lukyan.example.spring.boot.primefaces.jpa.entity.Contact;

import java.util.List;

public interface CustomContactRepository {
    List<Contact> findAll(String sortBy, String sortOrder,
                          Integer offset, Integer limit,
                          String keyword);
}