package com.lukyan.example.spring.boot.primefaces.validators;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;
import java.util.regex.Pattern;

@FacesValidator("phoneValidator")
public class PhoneValidator implements Validator {

    private static final Logger log = LoggerFactory.getLogger(PhoneValidator.class);

    private Pattern pattern;

    private static final String PHONE_PATTERN = "([0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9])";

    public PhoneValidator() {
        pattern = Pattern.compile(PHONE_PATTERN);
    }

    @Override
    public void validate(FacesContext context, UIComponent component, Object value) throws ValidatorException {
        if (value == null || "".equals(String.valueOf(value).trim())) {
            String summary = "Validation Error";
            String detail = "No value provided to validation";
            log.info(detail);
            FacesMessage facesMessage = new FacesMessage(FacesMessage.SEVERITY_ERROR, summary, detail);
            throw new ValidatorException(facesMessage);
        }

        if (!pattern.matcher(value.toString()).matches()) {
            String summary = "Validation Error";
            String detail = String.format("%s is not a valid phone", value);
            log.error(detail);
            FacesMessage facesMessage = new FacesMessage(FacesMessage.SEVERITY_ERROR, summary, detail);
            throw new ValidatorException(facesMessage);
        }
    }
}