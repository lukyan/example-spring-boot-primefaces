package com.lukyan.example.spring.boot.primefaces.services.api;

import com.lukyan.example.spring.boot.primefaces.jpa.entity.Contact;

import java.util.List;

public interface ContactService {
    Contact getOne(Long id);

    List<Contact> findAll(String sortBy, String sortOrder, Integer offset, Integer limit, String keyword);

    List<Contact> findAll();

    void delete(Long id);

    void delete(Contact contact);

    Contact save(Contact entity);

    Long count();
}
