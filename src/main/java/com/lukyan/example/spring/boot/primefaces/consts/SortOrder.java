package com.lukyan.example.spring.boot.primefaces.consts;

public interface SortOrder {
    String ASC = "asc";
    String DESC = "desc";
}
