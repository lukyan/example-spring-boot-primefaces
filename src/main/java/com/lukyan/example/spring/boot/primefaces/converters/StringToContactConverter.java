package com.lukyan.example.spring.boot.primefaces.converters;

import com.lukyan.example.spring.boot.primefaces.jpa.entity.Contact;
import lombok.NonNull;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.time.Instant;
import java.util.Arrays;
import java.util.List;

@Component
public class StringToContactConverter implements Converter<String, Contact> {
    @Override
    public Contact convert(@NonNull String str) {
        List<String> source = Arrays.asList(str.split("\\|"));
        Contact contact = new Contact();
        Long id = source.get(0) != null ? Long.valueOf(source.get(0)) : null;
        contact.setId(id);
        contact.setFirstName(source.get(1));
        contact.setLastName(source.get(2));
        contact.setPhone(source.get(3));
        String createdAtStr = source.get(4);
        Instant createdAt = createdAtStr != null ? Instant.parse(createdAtStr) : null;
        contact.setCreatedAt(createdAt);
        String updatedAtStr = source.get(5);
        Instant updatedAt = updatedAtStr != null ? Instant.parse(updatedAtStr) : null;
        contact.setUpdatedAt(updatedAt);
        return contact;
    }
}
