package com.lukyan.example.spring.boot.primefaces.consts;

public interface DlgOption {
    String resizable = "resizable";
    String draggable = "draggable";
    String modal = "modal";
    String height = "height";
    String contentHeight = "contentHeight";
}
