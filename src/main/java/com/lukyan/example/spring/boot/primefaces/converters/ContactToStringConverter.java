package com.lukyan.example.spring.boot.primefaces.converters;

import com.lukyan.example.spring.boot.primefaces.jpa.entity.Contact;
import lombok.NonNull;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class ContactToStringConverter implements Converter<Contact, String> {
    @Override
    public String convert(@NonNull Contact source) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(source.getId());
        stringBuilder.append("|");
        stringBuilder.append(source.getFirstName());
        stringBuilder.append("|");
        stringBuilder.append(source.getLastName());
        stringBuilder.append("|");
        stringBuilder.append(source.getPhone());
        stringBuilder.append("|");
        String createdAt = source.getCreatedAt() != null ? source.getCreatedAt().toString() : null;
        stringBuilder.append(createdAt);
        stringBuilder.append("|");
        String updatedAt = source.getUpdatedAt() != null ? source.getUpdatedAt().toString() : null;
        stringBuilder.append(updatedAt);
        return stringBuilder.toString();
    }
}
