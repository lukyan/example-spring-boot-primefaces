package com.lukyan.example.spring.boot.primefaces.comparators;

import com.lukyan.example.spring.boot.primefaces.jpa.entity.AbstractEntity;
import lombok.NonNull;

import java.util.Comparator;

public class AbstractEntityComparator implements Comparator<AbstractEntity> {
    @Override
    public int compare(@NonNull AbstractEntity first, @NonNull AbstractEntity second) {
        if (first.getId() < second.getId()) {
            return -1;
        } else if (first.getId() > second.getId()) {
            return 1;
        }
        return 0;
    }
}
