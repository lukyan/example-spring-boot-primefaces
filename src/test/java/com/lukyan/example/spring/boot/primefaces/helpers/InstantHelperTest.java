package com.lukyan.example.spring.boot.primefaces.helpers;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.Instant;

import static org.junit.Assert.assertNotNull;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class InstantHelperTest {

    private final static Logger log = LoggerFactory.getLogger(InstantHelperTest.class);

    @Test
    public void test001Convert() {
        Instant instant = InstantHelper.instance().convert("2015-04-28T14:23:38.521Z");
        assertNotNull(instant);
        String instantAsString = InstantHelper.instance().convert(instant);
        assertNotNull(instantAsString);
    }
}