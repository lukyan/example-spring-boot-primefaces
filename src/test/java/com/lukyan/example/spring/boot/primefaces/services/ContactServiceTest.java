package com.lukyan.example.spring.boot.primefaces.services;

import com.lukyan.example.spring.boot.primefaces.Application;
import com.lukyan.example.spring.boot.primefaces.jpa.entity.Contact;
import com.lukyan.example.spring.boot.primefaces.services.api.ContactService;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.Instant;
import java.util.List;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

@RunWith(SpringRunner.class)
/*
@SpringBootTest(classes = Application.class,
        webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT,
        properties = {"server.port=18087"})
*/
@SpringBootTest(classes = Application.class,
        webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class ContactServiceTest {

    @Autowired
    private ContactService contactService;

    private static Contact entity;

    @Before
    public void setUp() {
        Instant now = Instant.now();
        entity = new Contact();
        entity.setFirstName("Alexander");
        entity.setLastName("Lukyanenko");
        entity.setPhone("38050111111111");
        entity.setCreatedAt(now);
        entity.setUpdatedAt(now);
    }

    @Test
    public void test001ContactAddPositive001() {
        Contact contact = contactService.save(entity);
        assertNotNull(contact);
        assertNotNull(contact.getId());
    }

    @Test(expected = Throwable.class)
    public void test002ContactAddNegative002() {
        Contact contact = entity.clone();
        contact.setFirstName(null);
        contactService.save(contact);
    }

    @Test
    public void test003ContactDeletePositive003() {
        Long countBefore = contactService.count();
        contactService.delete(1L);
        Long countAfter = contactService.count();
        assertTrue((countBefore - countAfter) == 1);
    }

    @Test
    public void test004ContactFindAllPositive004() {
        List<Contact> contacts = contactService.findAll();
        assertNotNull(contacts);
        assertTrue(contacts.size() > 0);
    }
}